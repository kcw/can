import type { SvelteComponentTyped } from 'svelte'
import type { SupabaseClient } from '@supabase/supabase-js'

export interface AuthProps {
  supabaseClient: SupabaseClient
  view?: 'sign_in' | 'sign_up' | 'magic_link' | 'forgotten_password'
  classes?: string
  style?: string
}

export default class Auth extends SvelteComponentTyped<AuthProps> {}