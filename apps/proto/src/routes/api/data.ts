
const { data: departments } = await supabaseServerClient(session.accessToken)
.from('departments')
.select('*')

const { data: productAreas } = await supabaseServerClient(session.accessToken)
.from('product_areas')
.select('*')

const { data: projects } = await supabaseServerClient(session.accessToken)
.from('projects')
.select('*')

const { data: contracts } = await supabaseServerClient(session.accessToken)
.from('contracts')
.select('*')

const { data: jobs } = await supabaseServerClient(session.accessToken)
.from('jobs')
.select('*')

const { data: clients } = await supabaseServerClient(session.accessToken)
.from('organisations')
.select('*')